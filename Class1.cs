﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _SmartCheckerWeb
{
    public class ExactMatch : IPlainPattern
    {
        public string Name { get { return "Точное совпадение"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            string basicAnswer = basicAnswers[0];
            string answer = studentAnswers[0];
            return string.Equals(basicAnswer.Trim(), answer.Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Circle : IPlainPattern
    {
        public string Name { get { return "Цикл"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            string basicAnswer = basicAnswers[0];
            string answer = studentAnswers[0];
            var separator = new char[] { ' ', '\t', ',', ';', '-', '>' };
            string[] basicAnswerVert = basicAnswer.Split(separator);
            string[] answerVert = answer.Split(separator);

            var answerList = new LinkedList<string>(answerVert);
            answerList.Remove("");
            var basicAnswerList = new LinkedList<string>(basicAnswerVert);
            basicAnswerList.Remove("");

            if (answerList.Last() == answerList.First())
                answerList.RemoveLast();
            if (basicAnswerList.Last() == basicAnswerList.First())
                basicAnswerList.RemoveLast();

            if (basicAnswerList.Count != answerList.Count)
                return false;

            var length = basicAnswerList.Count;
            var clockwiseBypassList = new LinkedList<string>(answerList);
            var anticlockwiseBypassList = new LinkedList<string>(answerList.Reverse());


            for (var i = 0; i < length; i++)
            {
                if (basicAnswerList.SequenceEqual(clockwiseBypassList) ||
                    basicAnswerList.SequenceEqual(anticlockwiseBypassList))
                    return true;

                changeSequenceOrder(clockwiseBypassList);
                changeSequenceOrder(anticlockwiseBypassList);
            }
            return false;
        }

        void changeSequenceOrder(LinkedList<string> sequence)
        {
            var head = sequence.First();
            sequence.AddLast(head);
            sequence.RemoveFirst();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Set : IPlainPattern
    {
        public string Name { get { return "Перестановка"; } }

        public bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers)
        {
            string basicAnswer = basicAnswers[0].Trim();
            string answer = studentAnswers[0].Trim();
            var separator = new char[] { '\r', '\n', ' ', '-', '\t', ',', ';' };
            string[] basicAnswerVert = basicAnswer.Split(separator).Where(s => !string.IsNullOrEmpty(s)).ToArray();
            string[] answerVert = answer.Split(separator).Where(s => !string.IsNullOrEmpty(s)).ToArray();

            HashSet<string> basicSet = new HashSet<string>();
            foreach (var bav in basicAnswerVert)
                basicSet.Add(bav);
            HashSet<string> studentSet = new HashSet<string>();
            foreach (var sav in answerVert)
                studentSet.Add(sav);
            bool res = basicSet.SetEquals(studentSet);
            return res;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
